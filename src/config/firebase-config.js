import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBsHVE4yE03qkJUHazqLdwqhS7-KC3doZI",
  authDomain: "codersmonkeys-4e4d5.firebaseapp.com",
  projectId: "codersmonkeys-4e4d5",
  storageBucket: "codersmonkeys-4e4d5.appspot.com",
  messagingSenderId: "438287490339",
  appId: "1:438287490339:web:3cb01272d89d192eab0096"
};

const app = initializeApp(firebaseConfig);