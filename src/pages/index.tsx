import { Seo } from "@/ui/components/seo/seo";
import Head from "next/head";

export default function Home() {
  return (
    <>
      <Seo title='Coders Monkeys' description='Coders Monkeys web site'/>
      <h1>Coders Monkeys</h1>
    </>
  );
}
